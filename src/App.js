import React, { Component } from 'react';
import Service from "./Service";
import { Table, Container } from 'reactstrap';


const data = {
    flat: {
        id: 1,
        name_service: "Квартплата",
        cost: 123,
        total: 0,
    },
    electricity:{
        id: 2,
        name_service: "Электроерегия",
        cost: 0.6168,
        total: 0
    },
    gas: {
        id: 3,
        name_service: "Газ",
        cost: 2.598,
        total: 0
    },
    water: {
        id: 4,
        name_service: "Холодная вода",
        cost: 11.02,
        total: 0
    },
    heating:{
        id: 5,
        name_service: "Отопление",
        cost: 164,
        total: 0
    },
    total_cost:0
}

class App extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            total_cost:0
        }

    }
    updateTotalCost = (object) => {
        this.setState({total_cost: 0});
        let total_cost = 0;
        Object.keys(data).forEach(key=>
        {
            if(data[key].id === object.service_id)
            {
                data[key].total = 0;
                data[key].total = parseInt(object.the_cost);
            }
            if(data[key].total > 0){
                total_cost += data[key].total
            }
        });
      this.setState({total_cost: total_cost})

  };
  render() {
    return (
      <div className="App">

            <Container>
                <header className="App-header">
                     <h1 className={'text-center'}>Расчет ЖКХ в ДНР на Щорса 104/23</h1>
                    <Table bordered size="sm">
                        <thead>
                        <tr>
                            <th >Услуга</th>
                            <th>Цена</th>
                            <th>Начальные данные</th>
                            <th>Конечные данные</th>
                            <th>Разница</th>
                            <th>Стоимость</th>
                        </tr>
                        </thead>
                        <tbody>
                            <Service data={data.flat}  updateTotalCost={this.updateTotalCost} />
                            <Service data={data.electricity}  updateTotalCost={this.updateTotalCost}  />
                            <Service data={data.gas}  updateTotalCost={this.updateTotalCost} />
                            <Service data={data.water} updateTotalCost={this.updateTotalCost}  />
                            <Service data={data.heating} updateTotalCost={this.updateTotalCost}  />
                        </tbody>
                    </Table>
                    <h4 className={'text-right'}>Общая плата = {this.state.total_cost} руб</h4>
                </header>
            </Container>

      </div>
    );
  }
}

export default App;
