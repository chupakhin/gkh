import React,{Component} from 'react'
import PropTypes from 'prop-types';

class Service extends Component{
    constructor(props)
    {
        super(props);
        this.state = {
            initial_value: 0,
            last_value: 0,
            difference: 0,
            the_cost: 0
        };
        this.handleCountInitial = this.handleCountInitial.bind(this);
        this.handleCountLast = this.handleCountLast.bind(this);

    }
    handleCountInitial (event)
    {
        this.setState({initial_value: event.target.value});
    };
    handleCountLast (event)
    {
        let  difference = 0;
        let  the_cost = 0;

        if(this.state.initial_value !== 0 && event.target.value !== 0)
        {
            difference = event.target.value - this.state.initial_value;
            the_cost = this.props.data.cost * difference;
            the_cost = Math.ceil(the_cost);

            this.setState({difference: difference});
            this.setState({the_cost: the_cost});

            this.props.updateTotalCost({the_cost: the_cost, service_id: this.props.data.id})

        }else {
            this.setState({initial_value: 0});
            this.setState({last_value: 0});
            this.setState({difference: 0});
            this.setState({the_cost: 0});
        }
    };
    render(){
        return(
                    <tr>
                        <td>{this.props.data.name_service}</td>
                        <td>{this.props.data.cost}</td>
                        <td>
                            <input  onChange={this.handleCountInitial} type="text"/>
                        </td>
                        <td>
                            <input onChange={this.handleCountLast} type="text"/>
                        </td>
                        <td>
                            {this.state.difference}
                        </td>
                        <td>{this.state.the_cost} руб</td>
                    </tr>
        )
    }
}
Service.propTypes = {
    name_service: PropTypes.string,
    cost: PropTypes.number,
    id: PropTypes.number,
    total: PropTypes.number
};
export default Service
